#pragma once
#include "Includes.h"

class Terrain
{
public:
	Terrain();
	void Draw(const Camera& camera);

private:
	ModelTransform transform;
	Texture texture;
	Shader shader;
	Mesh mesh;

};

