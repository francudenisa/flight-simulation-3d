#include "Terrain.h"

Terrain::Terrain() :
	texture("./resources/textures/grass2.jpg"),
	mesh("./resources/objects/Terrain/terrain.obj"),
	shader("./resources/shaders/Shader")
{
	transform.SetScale(glm::vec3(10, 10, 10));
}

void Terrain::Draw(const Camera& camera)
{
	shader.Bind();
	shader.Update(transform, camera);
	texture.Bind(0);
	mesh.Draw();
}
