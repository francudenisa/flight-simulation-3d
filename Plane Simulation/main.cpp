#include <GL/glew.h>
#include <chrono>
#include <iostream>

#include "Window.h"
#include "Screen.h"

#include "Skybox.h"
#include "Terrain.h"


int main()
{
	// Window
	Window window(SCREEN_WIDTH, SCREEN_HEIGHT, "Plane Simulation - Grafica 3D");

	// Camera & ModelTransform
	Camera camera(glm::vec3(0, 500, 0), 70.0f, static_cast<float>(SCREEN_WIDTH) / SCREEN_HEIGHT, 0.01f, 10000.0f);
	//camera.RotateY(glm::radians(-30.0f));
	//camera.Pitch(glm::radians(0.45f));
	window.SetCamera(camera);

	//Skybox
	Skybox skybox;

	// 3D Models
	Terrain terrain;

	// Plane
	Mesh planeM("./resources/objects/Plane1/plane.obj");
	Shader planeS("./resources/shaders/LambertShader");
	Texture planeTx("./resources/objects/Plane1/CIRRUSTS19.JPG");
	ModelTransform planeTr;
	window.SetPlaneTransform(planeTr);


	camera.RotateY(glm::radians(-15.0f));
	camera.Pitch(glm::radians(20.0f));
	// Render loop
	while (!window.IsClosed())
	{
		window.Clear(0.0f, 0.0f, 0.0f);

		// Render skybox
		skybox.Draw(camera.GetView(), camera.GetProjection());

		// Terrain
		terrain.Draw(camera);

		// Plane
		planeS.Bind();
		planeTr.SetPosition(camera.GetPosition());
		planeS.Update(planeTr, camera);
		planeTx.Bind(0);
		planeM.Draw();

		window.Update();
	}

	return 0;
}
