#pragma once
#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <GL/glew.h>

#include "ModelTransform.h"
#include "Camera.h"

#include <glm/gtc/type_ptr.hpp>

class Shader
{
public:
	Shader(const std::string& fileName);
	void Init(const char* vertexPath, const char* fragmentPath);
	Shader(const char* vertexPath, const char* fragmentPath)
	{
		Init(vertexPath, fragmentPath);
	}
	virtual ~Shader();
	Shader(const Shader& other) = delete;
	Shader& operator=(const Shader& other) = delete;

	void Bind();
	void Update(const ModelTransform& transform, const Camera& camera);

	void setMat4(const char* ch, const glm::mat4& mat);

	void setInt(const char* ch, const int& i);

	void SetVec3(const std::string& name, const glm::vec3& value) const
	{
		glUniform3fv(glGetUniformLocation(ID, name.c_str()), 1, &value[0]);
	}
	void SetVec3(const std::string& name, float x, float y, float z) const
	{
		glUniform3f(glGetUniformLocation(ID, name.c_str()), x, y, z);
	}

	void SetFloat(const std::string& name, const float& value)
	{
		glUniform1f(glGetUniformLocation(ID, name.c_str()), value);
	}
	void SetMat4(const std::string& name, const glm::mat4& mat) const
	{
		glUniformMatrix4fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
	}
	void Use() const
	{
		glUseProgram(ID);
	}

private:
	std::string LoadShader(const std::string& fileName);
	void CheckShaderError(GLuint shader, GLuint flag, bool isProgram, const std::string& errorMessage);
	GLuint CreateShader(const std::string& text, unsigned int type);

private:
	static const unsigned s_numShaders = 2;

private:
	enum
	{
		TRANSFORM_U,

		NUM_UNIFORMS
	};

private:
	GLuint m_program;
	GLuint m_shaders[s_numShaders];
	GLuint m_uniforms[NUM_UNIFORMS];
	unsigned int ID;
};

