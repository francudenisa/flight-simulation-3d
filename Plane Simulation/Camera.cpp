#include "Camera.h"

glm::vec3& Camera::GetPosition()
{
	return pos;
}

float Camera::GetYaw() const
{
	return yaw;
}

float Camera::GetPitch() const
{
	return pitch;
}
